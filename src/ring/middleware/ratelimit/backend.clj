(ns ring.middleware.ratelimit.backend)

(defprotocol Backend
  (get-limit [self limit k])
  (reset-limits! [self hour])
  (get-time [self])
  (get-current-time [self])
  (available? [self]))

(ns ring.middleware.ratelimit.local-atom
  (:use [ring.middleware.ratelimit util backend]))

(defn- update-state [state limit k]
  (assoc state k
    (if-let [v (state k)] (inc v) 1)))

(deftype LocalAtomBackend [rate-map time-atom] Backend
  (get-limit [self limit k]
    ((swap! rate-map update-state limit k) k))
  (reset-limits! [self time]
    (reset! rate-map {})
    (reset! time-atom time))
  (get-time [self] @time-atom)
  (get-current-time [self] current-hour)
  (available? [self] true))

(def ^:private default-rate-map (atom {}))
(def ^:private default-time (atom (current-hour)))

(defn local-atom-backend
  ([] (local-atom-backend default-rate-map))
  ([rate-map] (LocalAtomBackend. rate-map default-time)))
